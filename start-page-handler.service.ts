import * as _ from 'lodash';
import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {EMPTY, forkJoin, Observable, of, fromEvent, iif as observableIif} from 'rxjs';
import {catchError, flatMap, switchMap, tap, share, take, timeout, map, mergeMap, filter} from 'rxjs/operators';

import {Mesh} from './models/model';

import {DevicePlatform, LocalStorageItem} from '@shared/enums';

import {NavigationPage} from '@core/navigation/navigation-page';
import {LocalStorageService, MeshService, MobileDiagnosticService, SettingService} from '@core/services';
import {ConsoleService} from '@core/services/ConsoleService';
import {BaseClass} from '@shared/base/base.class';
import {AppActions} from '@app/actions/app.action';
import {EntityCacheStorageService} from '@core/cache/entity-cache-storage.service';
import {ApiCacheConfigService, ApiCacheEntity, EntityCacheKey} from '@core/cache/api-cache-config.service';
import {IntervalUpdateService} from '@core/services/interval-update.service';
import {DraftMessagesService} from '@core/services/draft-messages.service';
import {OfflineStatus} from '@app/pages/offline-page/offline-status.enum';
import {DiagnosticService} from '@core/services/diagnostic.service';
import {FileCacheService} from '@core/cache/file-cache.service';
import {ExtendedHttpService} from './http.service';
import {UserService} from '@core/services/user/user.service';
import {UserApiService} from '@core/services/user/user-api.service';

declare const device;
declare const cordova;

export interface NavigationInfo {
  page: NavigationPage;
  data?: any;
}

@Injectable({
  providedIn: 'root',
})
export class StartPageHandlerService extends BaseClass {
  private readonly dueTimeout = 15000;

  constructor(
    private consoleService: ConsoleService,
    private localStorageService: LocalStorageService,
    private settingService: SettingService,
    private meshService: MeshService,
    private mobileDiagnosticService: MobileDiagnosticService,
    private entityCacheStorageService: EntityCacheStorageService,
    private intervalUpdateService: IntervalUpdateService,
    private apiCacheConfigService: ApiCacheConfigService,
    private draftMessagesService: DraftMessagesService,
    private diagnosticService: DiagnosticService,
    private fileCacheService: FileCacheService,
    private http: ExtendedHttpService,
    private userService: UserService,
    private userApiService: UserApiService
  ) {
    super();
  }

  public getStartPage(): Observable<NavigationInfo> {
    return of(navigator.onLine).pipe(
      switchMap((isOnline: boolean) => {
        if (isOnline) {
          return this.consoleService.heartbeat().pipe(timeout(this.dueTimeout));
        } else {
          AppActions.setRoute(NavigationPage.OFFLINE_PAGE);

          return EMPTY;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        AppActions.setRoute(NavigationPage.OFFLINE_PAGE, {status: OfflineStatus.TIS_DOWN});

        return EMPTY;
      }),
      switchMap(() => this.autoLogin())
    );
  }

  public getPageAfterLogin(skipWizard: boolean = false): Observable<NavigationInfo> {
    // Since we need to wait for "deviceready" event, URL workaround is used to allow testing in browser env
    const urls = ['http://localhost/', 'ionic://localhost/'];

    return observableIif(() => urls.includes(document.URL), fromEvent(document, 'deviceready'), of(null)).pipe(
      flatMap(() => {
        return skipWizard
          ? this.init()
          : this.isPermissionWizard().pipe(
              flatMap((shouldShowWizard: boolean) => {
                return shouldShowWizard ? this.runPermissionWizard() : this.init();
              })
            );
      })
    );
  }

  private init(): Observable<NavigationInfo> {
    const user = this.userService.getUserValue();
    const getDeviceTypes = this.consoleService.getDeviceTypes();
    const bindDevice = this.consoleService.bindDevice(user._id, this.consoleService.registrationId);

    if (typeof device !== 'undefined') {
      switch (device.platform) {
        case DevicePlatform.IOS:
          this.fileCacheService.initializeCache();
          break;
        case DevicePlatform.ANDROID:
          this.diagnosticService
            .getExternalStorageAuthorizationStatus()
            .pipe(
              mergeMap((status) =>
                status === cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED
                  ? this.diagnosticService.requestExternalStorageAuthorization()
                  : of(status)
              ),
              filter((status) => status === cordova.plugins.diagnostic.permissionStatus.GRANTED)
            )
            .subscribe((status: string) => {
              this.fileCacheService.initializeCache();
            });
          break;
      }
    }

    this.initAsyncMeshesUpdates();

    return forkJoin([getDeviceTypes, bindDevice]).pipe(
      switchMap(([, deviceContext]) => {
        // ensure anonymity matches the server-side state
        const deviceOs: DevicePlatform =
          typeof device !== 'undefined' ? (device.platform as DevicePlatform) : DevicePlatform.IOS;

        if (this.settingService.anonymity) {
          if (!deviceContext.anonymous) {
            this.settingService.activateAnonymity(
              this.consoleService.registrationId,
              this.consoleService.mesh ? this.consoleService.mesh._id : null,
              deviceOs
            );
          }
        } else {
          if (deviceContext.anonymous) {
            this.settingService.deactivateAnonymity(
              this.consoleService.registrationId,
              user ? user._id : null,
              this.consoleService.mesh ? this.consoleService.mesh._id : null,
              deviceOs
            );
          }
        }

        return this.afterInit();
      })
    );
  }

  private isPermissionWizard(): Observable<boolean> {
    if (typeof cordova === 'undefined' || typeof device === 'undefined') {
      return of(false);
    }

    const isAndroid = device.platform === DevicePlatform.ANDROID;
    const isIOS = device.platform === DevicePlatform.IOS;
    const location$ = this.diagnosticService.getLocationAuthorizationStatus();
    const notifications$ = isAndroid
      ? of(cordova.plugins.diagnostic.permissionStatus.GRANTED)
      : this.diagnosticService.getRemoteNotificationsAuthorizationStatus();
    // Note, that motionStatus is iOS only, so we are using permissionStatus as a workaround
    const motion$ = isAndroid
      ? of(cordova.plugins.diagnostic.permissionStatus.GRANTED)
      : this.diagnosticService.getMotionAuthorizationStatus();

    return forkJoin([location$, notifications$, motion$]).pipe(
      map((permissions) => {
        const notRequested = [
          cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED,
          cordova.plugins.diagnostic.permissionStatus.NOT_DETERMINED,
        ];

        if (isIOS) {
          notRequested.push(
            cordova.plugins.diagnostic.motionStatus.NOT_DETERMINED,
            cordova.plugins.diagnostic.motionStatus.NOT_REQUESTED
          );
        }

        return _.intersection(permissions, notRequested).length > 0;
      })
    );
  }

  private runPermissionWizard(): Observable<NavigationInfo> {
    return of({
      page: NavigationPage.WIZARD_PERMISSIONS_PAGE,
      data: {},
    });
  }

  /**
   * Determine whether we should stay on the Meshes page or navigate to the Mesh page.
   * Navigation to the Mesh page occurs in 2 next cases:
   * - there is only one mesh in the list;
   * - last mesh uuid is remembered, related mesh is available and consent agreement is accepted.
   */
  private afterInit(): Observable<NavigationInfo> {
    return this.entityCacheStorageService.get(EntityCacheKey.MESHES).pipe(
      take(1),
      tap((data) => {
        this.entityCacheStorageService.set(EntityCacheKey.MESHES, data);
      }),
      flatMap((meshes) => {
        if (_.isEmpty(meshes)) {
          return of({page: NavigationPage.MESHES_PAGE});
        }

        let mesh: any;

        if (meshes.length === 1) {
          mesh = meshes[0];
        } else {
          const lastMeshUuid = this.localStorageService.get(LocalStorageItem.LAST_MESH_UUID);
          mesh = _.find(meshes, {uuid: lastMeshUuid});
        }

        if (mesh) {
          this.http.routingParams = {
            customerId: 'string' === typeof mesh.customer ? mesh.customer : String(mesh.customer._id),
            meshId: String(mesh._id),
          };
          return this.meshService.getMesh(mesh._id).pipe(
            switchMap((detailMesh) => {
              const bindedMesh: any = Mesh.bind(this.consoleService.deviceTypes, detailMesh);

              return this.consoleService.checkConsent(bindedMesh).pipe(
                flatMap((agreed: boolean) => {
                  if (agreed) {
                    this.mobileDiagnosticService.runDiagnostics(bindedMesh);
                  }

                  this.removeFilesFromBufferOnIOS();

                  return agreed
                    ? of({
                        page: NavigationPage.MESH_PAGE,
                        data: {...bindedMesh, __updatedMesh: true},
                      })
                    : of({page: NavigationPage.MESHES_PAGE});
                })
              );
            })
          ) as Observable<NavigationInfo>;
        } else {
          return of({page: NavigationPage.MESHES_PAGE});
        }
      })
    );
  }

  private autoLogin(): Observable<NavigationInfo> {
    const token: string = this.localStorageService.get(LocalStorageItem.JWT_TOKEN);
    const rememberMe: boolean = this.localStorageService.get(LocalStorageItem.REMEMBER_ME);

    if (!token || !rememberMe) {
      return of({page: NavigationPage.LOGIN_PAGE});
    }

    let loginProvider;

    if (this.localStorageService.get(LocalStorageItem.LOGIN_PROVIDER) === null) {
      loginProvider = 'loginProvider';
    } else {
      loginProvider = this.localStorageService.get(LocalStorageItem.LOGIN_PROVIDER);
      if ('object' === typeof loginProvider && 0 === Object.keys(loginProvider).length) {
        loginProvider = 'loginProvider';
      }
    }

    if ('object' === typeof loginProvider && 0 === Object.keys(loginProvider).length) {
      loginProvider = null;
    }

    this.localStorageService.set(LocalStorageItem.LOGIN_PROVIDER, loginProvider);

    if (loginProvider && loginProvider === 'cobot') {
      return of({page: NavigationPage.LOGIN_PAGE});
      // Temporary workaround of AppStore approval issues with Cobot OAuth2 Page
    } else {
      return this.userApiService.getLoggedInUser().pipe(
        switchMap((loginResponse: any) => {
          if (loginResponse) {
            this.userService.setUser(loginResponse);
            this.consoleService.state = 'loggedIn';
          }

          return !loginResponse ? of({page: NavigationPage.LOGIN_PAGE}) : this.getPageAfterLogin();
        }),
        catchError(() => of({page: NavigationPage.LOGIN_PAGE}))
      );
    }
  }

  private initAsyncMeshesUpdates() {
    const meshes$ = this.meshService.getMeshes().pipe(
      tap((data) => {
        this.entityCacheStorageService.set(EntityCacheKey.MESHES, data);
      }),
      share()
    );
    const cacheEntity = this.apiCacheConfigService.updateApiCacheConfig(EntityCacheKey.MESHES, {
      callback: () => meshes$,
    } as ApiCacheEntity);

    this.intervalUpdateService.add({
      key: EntityCacheKey.MESHES,
      callback: cacheEntity.callback,
      period: cacheEntity.ttl,
    });
  }

  private removeFilesFromBufferOnIOS(): void {
    if (typeof device !== 'undefined' && device.platform === DevicePlatform.IOS) {
      this.draftMessagesService.removeFilesFromBuffer();
    }
  }
}
