import * as _ from 'lodash';
import {
  iif as observableIif,
  interval,
  Observable,
  Observer,
  of,
  timer,
  empty,
  BehaviorSubject,
  fromEvent,
  combineLatest,
} from 'rxjs';
import {Injectable, NgZone} from '@angular/core';
import {MatDialog} from '@angular/material';
import {
  catchError,
  filter,
  map,
  switchMap,
  publish,
  refCount,
  timeout,
  distinctUntilChanged,
  take,
} from 'rxjs/operators';
import {select} from '@angular-redux/store';
import {ActiveToast} from 'ngx-toastr';

import {NavigationPage} from '@core/navigation/navigation-page';
import {Layer} from '@shared/interfaces/layer.interface';
import {BaseClass} from '@shared/base/base.class';
import {IconType} from '@shared/components/search/search.enums';
import {EnforceMode} from '@shared/enums/enforce-mode.enum';
import {InternetConnectionType} from '@shared/enums/internet-connection-type.enum';
import {LogService} from './log.service';
import {Icon, NotificationService} from './notification.service';
import {
  BluetoothIssue,
  BluetoothState,
  DevicePlatform,
  LocationIssue,
  NetworkIssue,
  NotificationButtonTitle,
  NotificationMessage,
  PushNotificationIssue,
  SystemSettingType,
  VersionIssue,
} from '@shared/enums';
import {NotificationPositionClass} from '@app/shared/enums/notification-position.enum';
import {ApiService} from './api.service';
import {TelemetryService} from './telemetry/telemetry.service';
import {GeneralErrorEvent} from '@core/services/telemetry/event/events/general/general-error-event';
import {AppActions} from '@app/actions/app.action';

declare const cordova;
declare const device;
declare const Connection;
declare const navigator;

@Injectable()
export class MobileDiagnosticService extends BaseClass {
  private versionNumber: string;
  private poorNetwork: boolean;
  private isSlowConnectionNotificationAppeared = false;
  @select(['router', 'head']) private head$: Observable<Layer[]>;
  private currentPage: Layer;
  private readonly cordovaPluginsDiagnosticIsNotDefinedMessage: string = 'cordova.plugins.diagnostic is not defined';

  private serverOnline$: BehaviorSubject<boolean> = new BehaviorSubject(true);
  private deviceOnline$: BehaviorSubject<boolean> = new BehaviorSubject(true);
  private online$: Observable<boolean>;

  constructor(
    public dialog: MatDialog,
    private ngZone: NgZone,
    private notificationService: NotificationService,
    private logService: LogService,
    private apiService: ApiService,
    private telemetryService: TelemetryService
  ) {
    super();
    this.initServerPing();
    this.head$.pipe(filter(Boolean)).subscribe((head: Layer) => {
      this.currentPage = head;
    });

    this.online$ = combineLatest(this.isServerOnline(), this.isDeviceOnline()).pipe(
      map(([serverOnline, deviceOnline]: [boolean, boolean]) => serverOnline && deviceOnline)
    );

    this.sub = this.isOnline()
      .pipe(
        map(
          (value: boolean) =>
            (value ? {type: InternetConnectionType.ONLINE} : {type: InternetConnectionType.OFFLINE}) as Event
        )
      )
      .subscribe((event: Event) => {
        this.handleConnectionChange(event);

        if (event.type === InternetConnectionType.OFFLINE) {
          AppActions.popSpinner();
        }
      });
  }
  
  public initializeNetworkMonitoring() {    
    this.sub = fromEvent(document, InternetConnectionType.OFFLINE).subscribe(() => {
      this.setDeviceOnline(false);
    });

    this.sub = fromEvent(document, InternetConnectionType.ONLINE)
      .pipe(
        switchMap(() => {
          return this.isServerOnlineApi();
        })
      )
      .subscribe((serverAvailable) => {
        this.setServerOnline(serverAvailable);
        this.setDeviceOnline(true);
      });

    // Check every 15 seconds
    this.sub = timer(3000, 15000)
      .pipe(
        map(() => navigator['connection'].type),
        map((type: string) => type === Connection.CELL_2G || type === Connection.CELL)
      )
      .subscribe((poor: boolean) => {
        this.poorNetwork = poor;

        if (this.poorNetwork && !this.isSlowConnectionNotificationAppeared) {
          this.openSlowConnectionNotification();
        } else if (!this.poorNetwork) {
          this.notificationService.removeNotificationByName(NetworkIssue.POOR_NETWORK);
        }
      });
  }

  public isServerOnline(): Observable<boolean> {
    return this.serverOnline$.asObservable().pipe(distinctUntilChanged());
  }

  public isDeviceOnline(): Observable<boolean> {
    return this.deviceOnline$.asObservable().pipe(distinctUntilChanged());
  }

  public isOnline(): Observable<boolean> {
    return this.online$;
  }

  private setDeviceOnline(value: boolean): void {
    if (this.deviceOnline$.getValue() !== value) {
      this.deviceOnline$.next(value);
    }
  }

  private setServerOnline(value: boolean): void {
    if (this.serverOnline$.getValue() !== value) {
      this.serverOnline$.next(value);
    }
  }

  private openSlowConnectionNotification(): void {
    const icon: Icon = {
      path: 'assets/svg/mobile-diagnostic-notifications-icons/connection.svg',
      type: IconType.SVG,
    };

    const callback = (toastRef: ActiveToast<any>) => {
      toastRef.onAction.pipe(take(1)).subscribe(() => {
        this.isSlowConnectionNotificationAppeared = true;
      });
    };

    const notificationPosition = this.getNotificationPosition();

    this.notificationService.openActionNotification(
      NetworkIssue.POOR_NETWORK,
      NotificationButtonTitle.DISMISS,
      icon,
      null,
      callback,
      notificationPosition
    );
  }

  public runDiagnostics(mesh: any, alwaysShowDialog: boolean = true): void {
    if (!alwaysShowDialog || typeof cordova === 'undefined') {
      return;
    }

    this.ensureMobileCapabilities(mesh)
      .pipe(filter((issues: string[]) => !_.isEmpty(issues)))
      .subscribe((issues: string[]) => {
        this.ngZone.run(() => {
          _.forEach(issues, (issue: string) => {
            if (_.values(BluetoothIssue).includes(issue)) {
              this.openBluetoothNotification(issue as BluetoothIssue);
            }

            if (_.values(LocationIssue).includes(issue)) {
              this.openLocationNotification(issue as LocationIssue);
            }

            if (_.values(PushNotificationIssue).includes(issue)) {
              this.openPushNotification(issue as PushNotificationIssue);
            }

            if (_.values(VersionIssue).includes(issue)) {
              this.openVersionNotification(issue as VersionIssue);
            }

            if (_.values(NetworkIssue).includes(issue)) {
              this.openNetworkNotification(issue as NetworkIssue);
            }
          });
        });
      });
  }

  private openBluetoothNotification(issue: BluetoothIssue): void {
    const icon: Icon = {
      path: 'assets/svg/mobile-diagnostic-notifications-icons/bluetooth.svg',
      type: IconType.SVG,
    };
    const callback: (notification: any) => void = (notification: any) => {
      notification.onAction.subscribe((data: any) => {
        this.openSystemSettings(SystemSettingType.BLUETOOTH);
        data.removeAction();
      });
    };
    let buttonTitle: NotificationButtonTitle;

    switch (issue) {
      case BluetoothIssue.SWITCHED_OFF:
        buttonTitle = NotificationButtonTitle.ON;
        break;
      case BluetoothIssue.UNAUTHORIZED:
        buttonTitle = NotificationButtonTitle.ALLOW;
        break;
      case BluetoothIssue.UNKNOWN:
        buttonTitle = NotificationButtonTitle.FIX;
        break;
    }

    this.notificationService.openActionNotification(issue, buttonTitle, icon, null, callback);
  }

  private openLocationNotification(issue: LocationIssue): void {
    let icon: Icon;
    let callback: (notification: any) => void;

    if (
      issue === LocationIssue.UNAUTHORIZED ||
      issue === LocationIssue.UNKNOWN ||
      issue === LocationIssue.IS_NOT_AVAILABLE
    ) {
      icon = {
        path: 'assets/svg/mobile-diagnostic-notifications-icons/location.svg',
        type: IconType.SVG,
      };
      callback = (notification: any) => {
        notification.onAction.subscribe((data: any) => {
          this.openSystemSettings(
            device.platform === DevicePlatform.IOS ? SystemSettingType.LOCATION_IOS : SystemSettingType.LOCATION_ANDROID
          );
          data.removeAction();
        });
      };
      this.notificationService.openActionNotification(issue, NotificationButtonTitle.FIX, icon, null, callback);
    } else {
      icon = {
        path: 'assets/svg/mobile-diagnostic-notifications-icons/location2.svg',
        type: IconType.SVG,
      };
      callback = (notification: any) => {
        notification.onAction.subscribe((data: any) => {
          this.openSystemSettings(SystemSettingType.APPLICATION_DETAILS);
          data.removeAction();
        });
      };
      this.notificationService.openActionNotification(issue, NotificationButtonTitle.ALLOW, icon, null, callback);
    }
  }

  private openPushNotification(issue: PushNotificationIssue): void {
    const callback: (notification: any) => void = (notification: any) => {
      notification.onAction.subscribe((data: any) => {
        this.openSystemSettings(
          device.platform === DevicePlatform.IOS
            ? SystemSettingType.PUSH_NOTIFICATION_IOS
            : SystemSettingType.APPLICATION_DETAILS
        );
        data.removeAction();
      });
    };
    let icon: Icon;
    let buttonTitle: NotificationButtonTitle;

    if (issue === PushNotificationIssue.REMOTE_NOTIFICATIONS_NOT_ENABLED) {
      icon = {
        path: 'assets/svg/mobile-diagnostic-notifications-icons/notification.svg',
        type: IconType.SVG,
      };
      buttonTitle = NotificationButtonTitle.ALLOW;
    } else {
      icon = {
        path: 'assets/svg/mobile-diagnostic-notifications-icons/push-notification-not-determined.svg',
        type: IconType.SVG,
      };
      buttonTitle = NotificationButtonTitle.FIX;
    }

    this.notificationService.openActionNotification(issue, buttonTitle, icon, null, callback);
  }

  private openVersionNotification(issue: VersionIssue): void {
    const icon: Icon = {
      path: 'assets/svg/mobile-diagnostic-notifications-icons/version-update.svg',
      type: IconType.SVG,
    };
    const backgroundImg: string = 'assets/images/notifications/mask1_image.jpg';

    this.notificationService.openNoActionNotification(issue, null, icon, backgroundImg);
  }

  private openNetworkNotification(issue: NetworkIssue): void {
    const icon: Icon = {
      path: 'assets/svg/mobile-diagnostic-notifications-icons/connection.svg',
      type: IconType.SVG,
    };
    
    this.notificationService.openNoActionNotification(issue, null, icon, null);
  }

  public openSystemSettings(type: SystemSettingType): void {
    cordova.plugins.settings.open(type, _.noop, _.noop);
  }

  public ensureMobileCapabilities(mesh: any): Observable<string[]> {
    const issues = [];
    if (mesh.appSettings && typeof cordova.plugins.diagnostic !== 'undefined') {
      return observableIif(
        () => !mesh.appSettings.usesIndoorLocation,
        of(''),
        this.handleBluetoothState().pipe(
          catchError((error) => {
            this.logService.logError(`handleBluetoothState error: ${error}`);

            return of(null);
          })
        )
      ).pipe(
        switchMap((issue: BluetoothIssue) => {
          if (issue) {
            issues.push(issue);
          }

          return observableIif(
            () => !mesh.appSettings.usesIndoorLocation,
            of(''),
            this.handleIndoorLocationState(mesh).pipe(
              catchError((error) => {
                this.logService.logError(`handleIndoorLocationState error: ${error}`);

                return of(null);
              })
            )
          );
        }),
        switchMap((issue: LocationIssue) => {
          if (issue) {
            issues.push(issue);
          }

          return observableIif(
            () => !mesh.appSettings.usesPushNotification,
            of(''),
            this.handlePushNotificationsState().pipe(
              catchError((error) => {
                this.logService.logError(`handlePushNotificationsState error: ${error}`);

                return of(null);
              })
            )
          );
        }),
        map((issue: PushNotificationIssue) => {
          if (issue) {
            issues.push(issue);
          }

          if (this.versionNumber && mesh.appSettings.minimumVersion) {
            const currentVersion = this.versionNumber.split('.');
            const minimumVersion = mesh.appSettings.minimumVersion.split('.');

            if (currentVersion[0] < minimumVersion[0] || currentVersion[0] < minimumVersion[0]) {
              issues.push(VersionIssue.UPDATE_REQUIRED);
            }
          }

          if (this.poorNetwork) {
            issues.push(NetworkIssue.POOR_NETWORK);
          }

          return issues;
        })
      );
    } else {
      return of([]);
    }
  }

  private handlePushNotificationsState(): Observable<PushNotificationIssue> {
    if (typeof cordova === 'undefined') {
      return empty();
    }

    return Observable.create((observer: Observer<PushNotificationIssue>) => {
      if (typeof cordova.plugins.diagnostic === 'undefined') {
        observer.error(this.cordovaPluginsDiagnosticIsNotDefinedMessage);
      }

      cordova.plugins.diagnostic.isRemoteNotificationsEnabled(
        (state: boolean) => {
          if (!state) {
            if (device.platform === DevicePlatform.IOS) {
              cordova.plugins.diagnostic.requestRemoteNotificationsAuthorization(
                () => {
                  observer.next(null);
                  observer.complete();
                },
                (error) => {
                  observer.next(PushNotificationIssue.UNAUTHORIZED);
                  observer.complete();
                }
              );
            } else {
              observer.next(PushNotificationIssue.REMOTE_NOTIFICATIONS_NOT_ENABLED);
              observer.complete();
            }
          } else {
            observer.next(null);
            observer.complete();
          }
        },
        (error) => {
          observer.next(PushNotificationIssue.UNKNOWN);
          observer.complete();
        }
      );
    }).pipe(
      publish(),
      refCount()
    );
  }

  private handleIndoorLocationState(mesh?: any): Observable<LocationIssue> {
    if (typeof cordova === 'undefined') {
      return empty();
    }

    return Observable.create((observer: Observer<LocationIssue>) => {
      if (typeof cordova.plugins.diagnostic === 'undefined') {
        observer.error(this.cordovaPluginsDiagnosticIsNotDefinedMessage);
      }

      cordova.plugins.diagnostic.isLocationEnabled(
        (locationEnabled: boolean) => {
          if (!locationEnabled) {
            observer.next(LocationIssue.IS_NOT_AVAILABLE);
            observer.complete();
          } else {
            cordova.plugins.diagnostic.getLocationAuthorizationStatus(
              (authorizationStatus: string) => {
                // can't request authorization on iOS if authorization status is NOT_DETERMINED
                if (
                  device.platform === DevicePlatform.ANDROID ||
                  authorizationStatus === cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED
                ) {
                  cordova.plugins.diagnostic.requestLocationAuthorization(
                    (status: string) => {
                      this.handleRequestAuthorizationStatus(status, observer, mesh);
                    },
                    (error) => {
                      observer.next(LocationIssue.UNAUTHORIZED);
                      observer.complete();
                    },
                    cordova.plugins.diagnostic.locationAuthorizationMode.ALWAYS
                  );
                } else {
                  this.handleRequestAuthorizationStatus(authorizationStatus, observer, mesh);
                }
              },
              (error: string) => {
                observer.next(LocationIssue.UNKNOWN);
                observer.complete();
              }
            );
          }
        },
        (error) => {
          observer.next(LocationIssue.UNKNOWN);
          observer.complete();
        }
      );
    }).pipe(
      publish(),
      refCount()
    );
  }

  private handleRequestAuthorizationStatus(status: string, observer: Observer<LocationIssue>, mesh?: any): void {
    switch (status) {
      case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
        observer.next(LocationIssue.PERMISSION_NOT_REQUESTED);
        observer.complete();
        break;
      case cordova.plugins.diagnostic.permissionStatus.GRANTED:
        observer.next(null);
        observer.complete();
        break;
      case cordova.plugins.diagnostic.permissionStatus.DENIED_ONCE:
        observer.next(LocationIssue.PERMISSION_DENIED);
        observer.complete();
        break;
      case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
        if (_.get(mesh, 'appSettings.enforcementMode') === EnforceMode.ENFORCE_SETTINGS) {
          observer.next(LocationIssue.GRANTED_WHEN_IN_USE);
        } else {
          observer.next(null);
        }
        observer.complete();
        break;
      case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
        observer.next(LocationIssue.PERMISSION_DENIED_ALWAYS);
        observer.complete();
        break;
      default:
        observer.next(LocationIssue.PERMISSION_STATUS_UNKNOWN);
        observer.complete();
    }
  }

  private handleBluetoothState(): Observable<BluetoothIssue> {
    if (typeof cordova === 'undefined') {
      return empty();
    }

    return Observable.create((observer: Observer<BluetoothIssue>) => {
      if (typeof cordova.plugins.diagnostic === 'undefined') {
        observer.error(this.cordovaPluginsDiagnosticIsNotDefinedMessage);
      }

      cordova.plugins.diagnostic.getBluetoothState(
        (state: BluetoothState) => {
          if (state === BluetoothState.POWERED_OFF) {
            observer.next(BluetoothIssue.SWITCHED_OFF);
            observer.complete();
          } else if (state === BluetoothState.UNAUTHORIZED) {
            // ios only
            cordova.plugins.diagnostic.requestBluetoothAuthorization(
              () => {
                observer.next(null);
                observer.complete();
              },
              (error) => {
                observer.next(BluetoothIssue.UNAUTHORIZED);
                observer.complete();
              }
            );
          } else {
            observer.next(null);
            observer.complete();
          }
        },
        (error) => {
          observer.next(BluetoothIssue.UNKNOWN);
          observer.complete();
        }
      );
    }).pipe(
      publish(),
      refCount()
    );
  }

  private handleConnectionChange(event: Event): void {
    if (event.type === InternetConnectionType.OFFLINE) {
      this.logService.logInfo('You lost connection.');
      this.openNoInternetNotification();
    }

    if (event.type === InternetConnectionType.ONLINE) {
      this.notificationService.removeNotificationByName(NotificationMessage.NO_INTERNET_CONNECTION);
      this.logService.logInfo('You are now back online.');
    }
  }

  private openNoInternetNotification(): void {
    if (this.currentPage.component.name === NavigationPage.OFFLINE_PAGE) {
      return;
    }

    const notificationPosition = this.getNotificationPosition();

    const icon: Icon = {
      path: 'assets/svg/mobile-diagnostic-notifications-icons/connection.svg',
      type: IconType.SVG,
    };
    const callback: (notification: any) => void = (notification: any) => {
      notification.onAction.subscribe((data: any) => {
        const animationDelay = 400;
        const source = timer(animationDelay);
        data.removeAction();

        this.sub = source.subscribe(() => {
          navigator.splashscreen.show();
          window.location.href = 'index.html';
        });
      });
    };

    const notificationRef = this.notificationService.openInfoNotification(
      NotificationMessage.NO_INTERNET_CONNECTION,
      NotificationButtonTitle.RELOAD,
      icon,
      null,
      callback,
      notificationPosition
    );

    return notificationRef;
  }

  private getNotificationPosition(): NotificationPositionClass {
    if (!this.currentPage || this.currentPage.component.name === NavigationPage.LOGIN_PAGE) {
      return NotificationPositionClass.TOP;
    } else {
      return NotificationPositionClass.UNDER_TOP_BAR;
    }
  }

  private initServerPing() {
    this.sub = timer(5000, 30000)
      .pipe(
        switchMap(() => {
          return this.isServerOnlineApi();
        })
      )
      .subscribe((serverAvailable) => {
        this.setServerOnline(serverAvailable);
      });
  }

  private isServerOnlineApi(): Observable<any> {
    return this.apiService.get('heartbeat').pipe(
      map(() => true),
      timeout(20000),
      catchError((error) => {
        this.telemetryService.log(new GeneralErrorEvent(error, 'Server is not available!!!'));

        return of(false);
      })
    );
  }
}
