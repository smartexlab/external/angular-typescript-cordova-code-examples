import {Observable} from 'rxjs';

import {PermissionId} from '@app/pages/wizard-permissions/permission-id.enum';

export interface ActivatePermission {
  activate: () => Observable<string>;
}

export interface PermissionStatus {
  status?: string;
  status$: Observable<string>;
}

export interface Priority {
  priority: number;
}

export interface Permission extends ActivatePermission, PermissionStatus, Priority {
  id: PermissionId;
  title: string;
  description: string;
  image: string;
}
