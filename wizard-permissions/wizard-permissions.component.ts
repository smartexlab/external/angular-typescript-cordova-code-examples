import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import {forkJoin} from 'rxjs';
import {map, tap} from 'rxjs/operators';

import {NavigationInfo, StartPageHandlerService} from '@core/services/start-page-handler.service';
import {NavigationPage} from '@core/navigation/navigation-page';
import {AppActions} from '@app/actions/app.action';
import {DiagnosticService} from '@core/services/diagnostic.service';
import {FileCacheService} from '@core/cache/file-cache.service';
import {LogService} from '@core/services/log.service';
import {Permission} from '@app/pages/wizard-permissions/permission.interface';
import {PermissionId} from '@app/pages/wizard-permissions/permission-id.enum';
import {DevicePlatform} from '@shared/enums';

declare const device;
declare const cordova;
declare const window;
declare const navigator;

@Component({
  selector: 'ti-wizard-permissions',
  templateUrl: './wizard-permissions.component.html',
  styleUrls: ['./wizard-permissions.component.less'],
})
export class WizardPermissionsComponent implements OnInit, AfterViewInit, OnDestroy {
  public permissions: Permission[];
  public activePermission: Permission;
  public activePermissionIndex: number;

  private isIOS: boolean = true;
  private eventListeners = [];

  constructor(
    private startPageHandlerService: StartPageHandlerService,
    private diagnosticService: DiagnosticService,
    private logService: LogService,
    private fileCacheService: FileCacheService
  ) {}

  public ngOnInit(): void {
    this.isIOS = typeof device !== 'undefined' && device.platform === DevicePlatform.IOS;

    this.initPermissions();
  }

  public ngOnDestroy(): void {
    this.eventListeners.forEach((dispose) => dispose());
  }

  public ngAfterViewInit() {
    const hideSplashscreen = () => {
      if (navigator && navigator.splashscreen) {
        navigator.splashscreen.hide();
      }
    };

    document.addEventListener('deviceready', hideSplashscreen, false);
    this.eventListeners.push(() => document.removeEventListener('deviceready', hideSplashscreen));
  }

  public onActivate(id: PermissionId): void {
    this.completePermission(id);
  }

  public skip(): void {
    this.completeWizard();
  }

  private initPermissions(): void {
    const notRequested = [
      cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED,
      cordova.plugins.diagnostic.permissionStatus.NOT_DETERMINED,
    ];

    if (this.isIOS) {
      notRequested.push(
        cordova.plugins.diagnostic.motionStatus.NOT_DETERMINED,
        cordova.plugins.diagnostic.motionStatus.NOT_REQUESTED
      );
    }

    const platformSpecificPermissions = this.getDeviceSpecificPermissions(this.getAllAvailablePermissions());

    forkJoin(
      platformSpecificPermissions.map((permission: Permission) =>
        permission.status$.pipe(map((status: string) => ({status, ...permission})))
      )
    )
      .pipe(
        map((permissions: Permission[]) =>
          permissions.filter((permission: Permission) => notRequested.includes(permission.status))
        ),
        map((permissions: Permission[]) => permissions.sort((a: Permission, b: Permission) => a.priority - b.priority))
      )
      .subscribe((permissions: Permission[]) => {
        this.permissions = permissions;
        this.activePermission = permissions[0];
        this.activePermissionIndex = 0;
      });
  }

  private completePermission(id: PermissionId): void {
    const currentPermissionIndex: number = this.permissions.findIndex((permission: Permission) => permission.id === id);
    const nextPermission = this.permissions[currentPermissionIndex + 1];

    if (nextPermission) {
      this.activePermission = nextPermission;
      this.activePermissionIndex = currentPermissionIndex + 1;
    } else {
      this.completeWizard();
    }
  }

  private getDeviceSpecificPermissions(permissions: Map<PermissionId, Permission>): Permission[] {
    const ids: PermissionId[] = this.isIOS
      ? [PermissionId.LOCATION, PermissionId.MOTION, PermissionId.PUSH_NOTIFICATIONS]
      : [PermissionId.LOCATION, PermissionId.STORAGE];

    return ids.map((id: PermissionId) => permissions.get(id));
  }

  private getAllAvailablePermissions(): Map<PermissionId, Permission> {
    const permissions = new Map<PermissionId, Permission>();
    permissions.set(PermissionId.LOCATION, this.getLocationPermission());
    permissions.set(PermissionId.MOTION, this.getMotionPermission());
    permissions.set(PermissionId.PUSH_NOTIFICATIONS, this.getPushNotificationPermission());
    permissions.set(PermissionId.STORAGE, this.getStoragePermission());

    return permissions;
  }

  private getStoragePermission(): Permission {
    return {
      id: PermissionId.STORAGE,
      title: 'Storage Permissions',
      image: 'assets/images/wizard-permissions/storage.png',
      description: 'Application wants to cache image and other files locally to avoid network traffic',
      activate: () =>
        this.diagnosticService.requestExternalStorageAuthorization().pipe(
          tap((status: string) => {
            if (status === cordova.plugins.diagnostic.permissionStatus.GRANTED) {
              this.fileCacheService.initializeCache();

              const dataPath: string = '.app/data';

              const folderPath = `${cordova.file.externalRootDirectory}${dataPath}`;
              const filename = '.nomedia';

              window.resolveLocalFileSystemURL(folderPath, (dir: any) => {
                dir.getFile(filename, {create: true}, (fileEntry) => {
                  this.logService.logInfo(`${folderPath}/${filename} was created`);
                });
              });
            }
          })
        ),
      status$: this.diagnosticService.getExternalStorageAuthorizationStatus(),
      priority: 5,
    };
  }

  private getPushNotificationPermission(): Permission {
    return {
      id: PermissionId.PUSH_NOTIFICATIONS,
      title: 'Push Notifications',
      image: 'assets/images/wizard-permissions/push-notifications.png',
      description:
        'Application wants to send you Push Notifications e.g. to update you on new chat messages, remind you of meetings or help you saving energy in the building.',
      activate: () => this.diagnosticService.requestRemoteNotificationsAuthorization(),
      status$: this.diagnosticService.getRemoteNotificationsAuthorizationStatus(),
      priority: 5,
    };
  }

  private getMotionPermission(): Permission {
    return {
      id: PermissionId.MOTION,
      title: 'Motion/Fitness Data',
      image: 'assets/images/wizard-permissions/motion.png',
      description:
        'Application wants to know whether your phone is moving to optimize battery usage on Indoor Location and Point of Interest Ranging.',
      activate: () => this.diagnosticService.requestMotionAuthorization(),
      status$: this.diagnosticService.getMotionAuthorizationStatus(),
      priority: 3,
    };
  }

  private getLocationPermission(): Permission {
    return {
      id: PermissionId.LOCATION,
      title: 'Location Services',
      image: 'assets/images/wizard-permissions/location.png',
      description:
        'Application wants to access Location Services to determine your Indoor Location (you can opt out at any time) or find Points of Interest.',
      activate: () => this.diagnosticService.requestLocationAuthorization(),
      status$: this.diagnosticService.getLocationAuthorizationStatus(),
      priority: 1,
    };
  }

  private completeWizard(): void {
    this.startPageHandlerService.getPageAfterLogin(true).subscribe((info: NavigationInfo) => {
      switch (info.page) {
        case NavigationPage.MESH_PAGE:
          AppActions.setRoutes([
            {
              component: NavigationPage.LOGIN_PAGE,
              instance: {},
            },
            {
              component: NavigationPage.MESHES_PAGE,
              instance: {},
            },
            {
              component: NavigationPage.MESH_PAGE,
              instance: {mesh: info.data, id: 'mesh'},
            },
          ]);
          break;
        case NavigationPage.MESHES_PAGE:
          AppActions.setRoutes([
            {
              component: NavigationPage.LOGIN_PAGE,
              instance: {},
            },
            {
              component: NavigationPage.MESHES_PAGE,
              instance: {},
            },
          ]);
          break;
      }
    });
  }
}
