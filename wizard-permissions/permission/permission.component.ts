import {ChangeDetectionStrategy, Component, Input, Output, EventEmitter, NgZone} from '@angular/core';
import {Observable} from 'rxjs';
import {finalize} from 'rxjs/operators';

import {PermissionId} from '@app/pages/wizard-permissions/permission-id.enum';

@Component({
  selector: 'ti-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PermissionComponent {
  @Input() public id: PermissionId;
  @Input() public image: string;
  @Input() public title: string;
  @Input() public description: string;
  @Input() private activate: () => Observable<string>;
  @Output() private activated: EventEmitter<void> = new EventEmitter<void>();

  constructor(private ngZone: NgZone) {}

  public activatePermission(): void {
    if (!this.activate) {
      this.emitActivationInZone();

      return;
    }

    this.activate()
      .pipe(finalize(() => this.emitActivationInZone()))
      .subscribe(() => {});
  }

  private emitActivationInZone(): void {
    this.ngZone.run(() => {
      this.activated.emit();
    });
  }
}
