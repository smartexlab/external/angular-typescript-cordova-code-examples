import {TestBed} from '@angular/core/testing';
import {NgZone} from '@angular/core';
import {Observer, of} from 'rxjs';
import {TestCtx, configureTestSuite, createStableTestContext} from 'ng-bullet';

import {PermissionComponent} from './permission.component';
import {LogService} from '@core/services/log.service';
import {MockNgZone, MockLogService} from '@testing/fake';
import {TranslateTestingModule} from '@testing/fake/ngx-translate/translate-testing.module';
import {click} from '@testing/utils/click';
import {SpyObserverFactory} from '@testing/utils/spy-observer.factory';

describe('PermissionComponent', () => {
  let context: TestCtx<PermissionComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule],
      declarations: [PermissionComponent],
      providers: [{provide: NgZone, useClass: MockNgZone}, {provide: LogService, useValue: MockLogService().Object}],
    });
  });

  beforeEach(async () => {
    context = await createStableTestContext(PermissionComponent);
  });

  it('should create permission component', () => {
    expect(context.component).toBeTruthy();
  });

  describe('when `activate` button is clicked', () => {
    let activateButton;
    let activatedSpyObserver: Observer<void>;

    beforeEach(() => {
      activateButton = context.element.querySelector('.activateButton') as HTMLElement;
      activatedSpyObserver = SpyObserverFactory.create<void>();
      context.component['activated'].subscribe(activatedSpyObserver);
    });

    it('should emit activation event if no callback is passed', () => {
      click(activateButton);

      expect(activatedSpyObserver.next).toHaveBeenCalledWith(undefined);
      expect(activatedSpyObserver.next).toHaveBeenCalledTimes(1);
      expect(activatedSpyObserver.error).not.toHaveBeenCalled();
    });

    it('should emit activation event if invalid callback is passed', () => {
      context.component['activate'] = null;

      click(activateButton);

      expect(activatedSpyObserver.next).toHaveBeenCalledWith(undefined);
      expect(activatedSpyObserver.next).toHaveBeenCalledTimes(1);
      expect(activatedSpyObserver.error).not.toHaveBeenCalled();
    });

    it('should run callback and emit activation event if valid callback is passed', () => {
      let activateSpyObserver: Observer<string> = SpyObserverFactory.create<string>();
      context.component['activate'] = () => of('');
      context.component['activate']().subscribe(activateSpyObserver);

      click(activateButton);

      expect(activatedSpyObserver.next).toHaveBeenCalledWith(undefined);
      expect(activatedSpyObserver.next).toHaveBeenCalledTimes(1);
      expect(activatedSpyObserver.error).not.toHaveBeenCalled();

      expect(activateSpyObserver.next).toHaveBeenCalledTimes(1);
      expect(activateSpyObserver.error).not.toHaveBeenCalled();
    });
  });
});
