import {TestBed} from '@angular/core/testing';
import {of} from 'rxjs';
import {configureTestSuite, createTestContext, TestCtx} from 'ng-bullet';
import {MockComponent} from 'ng-mocks';
import {Mock} from 'ts-mocks';

import {WizardPermissionsComponent} from './wizard-permissions.component';
import {TranslateTestingModule} from '@testing/fake/ngx-translate/translate-testing.module';
import {PermissionComponent} from '@app/pages/wizard-permissions/permission/permission.component';
import {StartPageHandlerService} from '@core/services/start-page-handler.service';
import {MockStartPageHandlerService} from '@testing/fake';
import {DiagnosticService} from '@core/services/diagnostic.service';
import {TopBarComponent} from '@app/top-bar/top-bar.component';
import {DevicePlatform} from '@shared/enums';
import {PermissionId} from '@app/pages/wizard-permissions/permission-id.enum';
import {AppActions} from '@app/actions/app.action';
import {NavigationPage} from '@core/navigation/navigation-page';
import {click} from '@testing/utils/click';

describe('WizardPermissionsComponent', () => {
  let context: TestCtx<WizardPermissionsComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule],
      declarations: [WizardPermissionsComponent, MockComponent(PermissionComponent), MockComponent(TopBarComponent)],
      providers: [
        {provide: StartPageHandlerService, useValue: MockStartPageHandlerService().Object},
        {provide: DiagnosticService, useValue: new Mock<DiagnosticService>().Object},
      ],
    });
  });

  beforeEach(() => {
    context = createTestContext(WizardPermissionsComponent);
  });

  it('should create wizard permissions component', () => {
    context.detectChanges();

    expect(context.component).toBeTruthy();
  });

  describe('when component is initialised', () => {
    it('should contain non-empty permissions array ', () => {
      context.detectChanges();
      const permissions = context.component.permissions;

      expect(permissions).toBeTruthy('Permissions array should be initialised');
      expect(permissions.length).toBeGreaterThan(0, 'Permissions array should contain at least one permission');
    });

    describe('and os is iOS', () => {
      let permissions;

      beforeEach(() => {
        window['device'] = {platform: DevicePlatform.IOS};
        context.detectChanges();
        permissions = context.component.permissions || [];
      });

      it('should show `location` permission as an active one', () => {
        const activePermission = context.component.activePermission;
        const activePermissionIndex = context.component.activePermissionIndex;

        expect(activePermissionIndex).toBe(0, 'Location permission should be the first displayed in the wizard');
        expect(activePermission.id).toBe(PermissionId.LOCATION, 'Location should be an active permission');
      });

      it('should contain `location` permission', () => {
        const locationPermission = permissions.find((p) => p.id === PermissionId.LOCATION);

        expect(locationPermission).toBeTruthy('Location permission should exist');
      });

      it('should contain `motion` permission', () => {
        const motionPermission = permissions.find((p) => p.id === PermissionId.MOTION);

        expect(motionPermission).toBeTruthy('Motion permission should exist');
      });

      it('should contain `push notification` permission', () => {
        const pushNotificationPermission = permissions.find((p) => p.id === PermissionId.PUSH_NOTIFICATIONS);

        expect(pushNotificationPermission).toBeTruthy('Push notification permission should exist');
      });

      it('should contain stepper', () => {
        const stepper = context.element.querySelector('.stepper');
        const steps = stepper.querySelectorAll('.step');

        expect(stepper).toBeTruthy('Stepper should be present in the DOM');
        expect(steps).toBeTruthy('Steps should be present in the DOM');
        expect(steps.length).toBe(permissions.length, 'Steps length should be the same as number of permissions');
      });
    });

    describe('and os is Android', () => {
      let permissions;

      beforeEach(() => {
        window['device'] = {platform: DevicePlatform.ANDROID};
        context.detectChanges();
        permissions = context.component.permissions || [];
      });

      it('should show `location` permission as an active one', () => {
        const activePermission = context.component.activePermission;
        const activePermissionIndex = context.component.activePermissionIndex;

        expect(activePermissionIndex).toBe(0, 'Location permission should be the first displayed in the wizard');
        expect(activePermission.id).toBe(PermissionId.LOCATION, 'Location should be an active permission');
      });

      it('should contain `location` permission', () => {
        const locationPermission = permissions.find((p) => p.id === PermissionId.LOCATION);

        expect(locationPermission).toBeTruthy('Location permission should exist');
      });
    });
  });

  describe('when `skip` is pressed', () => {
    beforeEach(() => {
      context.detectChanges();
    });

    it('should complete wizard', () => {
      spyOn(context.component, 'completeWizard' as any).and.stub();
      const skip = context.element.querySelector('.rightContent button') as HTMLElement;
      click(skip);

      expect(context.component['completeWizard']).toHaveBeenCalledTimes(1);
    });

    it('should set routes to meshes page if mesh is not defined', () => {
      const startPageHandlerService: StartPageHandlerService = context.resolve(StartPageHandlerService);
      startPageHandlerService.getPageAfterLogin = () =>
        of({
          page: NavigationPage.MESHES_PAGE,
          data: {},
        });
      spyOn(AppActions, 'setRoutes').and.stub();
      const skip = context.element.querySelector('.rightContent button') as HTMLElement;
      click(skip);
      const expectedRoutes = [
        {
          component: NavigationPage.LOGIN_PAGE,
          instance: {},
        },
        {
          component: NavigationPage.MESHES_PAGE,
          instance: {},
        },
      ];

      expect(AppActions.setRoutes).toHaveBeenCalledTimes(1);
      expect(AppActions.setRoutes).toHaveBeenCalledWith(expectedRoutes);
    });

    it('should set routes to mesh page if mesh is defined', () => {
      const startPageHandlerService: StartPageHandlerService = context.resolve(StartPageHandlerService);
      startPageHandlerService.getPageAfterLogin = () =>
        of({
          page: NavigationPage.MESH_PAGE,
          data: {},
        });
      spyOn(AppActions, 'setRoutes').and.stub();
      const skip = context.element.querySelector('.rightContent button') as HTMLElement;
      click(skip);
      const expectedRoutes = [
        {
          component: NavigationPage.LOGIN_PAGE,
          instance: {},
        },
        {
          component: NavigationPage.MESHES_PAGE,
          instance: {},
        },
        {
          component: NavigationPage.MESH_PAGE,
          instance: {mesh: {}},
        },
      ];

      expect(AppActions.setRoutes).toHaveBeenCalledTimes(1);
      expect(AppActions.setRoutes).toHaveBeenCalledWith(expectedRoutes);
    });
  });

  describe('when permission is activated', () => {
    describe('and os is iOS', () => {
      beforeEach(() => {
        window['device'] = {platform: DevicePlatform.IOS};
        context.detectChanges();
      });

      it('should complete `location` permission and show `motion` next', () => {
        spyOn(context.component, 'completePermission' as any).and.callThrough();
        spyOn(context.component, 'completeWizard' as any).and.stub();
        context.component.onActivate(PermissionId.LOCATION);

        const activePermission = context.component.activePermission;
        const activePermissionIndex = context.component.activePermissionIndex;

        expect(activePermissionIndex).toBe(1, 'Motion permission should be active');
        expect(activePermission.id).toBe(PermissionId.MOTION, 'Motion should be an active permission');
        expect(context.component['completePermission']).toHaveBeenCalledTimes(1);
        expect(context.component['completeWizard']).not.toHaveBeenCalled();
      });

      it('should complete `motion` permission and show `push notifications` next', () => {
        spyOn(context.component, 'completePermission' as any).and.callThrough();
        spyOn(context.component, 'completeWizard' as any).and.stub();

        context.component.onActivate(PermissionId.MOTION);

        const activePermission = context.component.activePermission;
        const activePermissionIndex = context.component.activePermissionIndex;

        expect(activePermissionIndex).toBe(2, 'Push notifications permission should be active');
        expect(activePermission.id).toBe(
          PermissionId.PUSH_NOTIFICATIONS,
          'Push notifications should be an active permission'
        );
        expect(context.component['completePermission']).toHaveBeenCalledTimes(1);
        expect(context.component['completeWizard']).not.toHaveBeenCalled();
      });

      it('should complete `push notifications` permission and complete wizard then', () => {
        spyOn(context.component, 'completePermission' as any).and.callThrough();
        spyOn(context.component, 'completeWizard' as any).and.stub();

        context.component.onActivate(PermissionId.PUSH_NOTIFICATIONS);

        expect(context.component['completePermission']).toHaveBeenCalledTimes(1);
        expect(context.component['completeWizard']).toHaveBeenCalledTimes(1);
      });
    });

    describe('and os is Android', () => {
      beforeEach(() => {
        window['device'] = {platform: DevicePlatform.ANDROID};
        context.detectChanges();
      });

      it('should complete `location` permission and show `storage` next', () => {
        spyOn(context.component, 'completePermission' as any).and.callThrough();
        spyOn(context.component, 'completeWizard' as any).and.stub();
        context.component.onActivate(PermissionId.LOCATION);

        const activePermission = context.component.activePermission;
        const activePermissionIndex = context.component.activePermissionIndex;

        expect(activePermissionIndex).toBe(1, 'Storage permission should be active');
        expect(activePermission.id).toBe(PermissionId.STORAGE, 'Storage should be an active permission');

        expect(context.component['completePermission']).toHaveBeenCalledTimes(1);
        expect(context.component['completeWizard']).not.toHaveBeenCalled();
      });

      it('should complete `storage` permission and complete wizard', () => {
        spyOn(context.component, 'completePermission' as any).and.callThrough();
        spyOn(context.component, 'completeWizard' as any).and.stub();

        context.component.onActivate(PermissionId.STORAGE);

        expect(context.component['completePermission']).toHaveBeenCalledTimes(1);
        expect(context.component['completeWizard']).toHaveBeenCalledTimes(1);
      });
    });
  });
});
