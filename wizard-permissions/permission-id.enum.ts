export enum PermissionId {
  LOCATION = 'location',
  MOTION = 'motion',
  PUSH_NOTIFICATIONS = 'pushNotifications',
  STORAGE = 'storage',
}
